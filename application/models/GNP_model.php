<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GNP_model extends CI_Model
{

	public function __construct()
	{
        	parent::__construct();
	}

	
        public function get_datos($datos){

                $db1 = $this->load->database('default', TRUE);

                $sql = "SELECT prim.prima_anual as anual, prim.prima_mensual as mensual 
                        FROM tbl_primas as prim
                        JOIN Estados as edos
                        ON prim.idciudad= edos.idestado
                        JOIN Genero as gen
                        ON prim.genero = gen.nombre
                        where prim.idciudad = ? and prim.genero = ? and prim.edad= ? and prim.opcion=? ";
                    
            
                $query = $db1->query($sql,array($datos['ciudad'],$datos['genero'],$datos['edad'],
                        'OPCION 1'));

                return $query->result_array();

        }


        public function insertaSolicitud($datos){

                $db1 = $this->load->database('default', TRUE);
                
                $sql = "INSERT INTO solicitudes(nombre, edad, sexo, ciudad, correo, telefono,fecha_solicitud) VALUES (?,?,?,?,?,?,NOW())";

                $result = $db1->query($sql,array($datos['nombre'],$datos['edad'],$datos['genero'],$datos['name_ciudad'],$datos['correo'],$datos['telefono']));

                $db2 = $this->load->database('crm_db', TRUE);
                $id_pagina = 5;

                $sql2 = 'INSERT INTO prospectos (idprocedencia, nombre, edad, correo, sexo, telefono, ciudad, fecha_registro) VALUES(?,?,?,?,?,?, ?, NOW())';

                $result2 = $db2->query($sql2,array($id_pagina, $datos['nombre'],$datos['edad'],$datos['correo'], $datos['sexo'], $datos['telefono'], $datos['name_ciudad']));


                return $result;

        }




}

