<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('GNP_model');
    }

    public function index() {
        $data["title"] = "GNP | Gastos Médicos Mayores";
        $data["view_file"] = "home";
        $this->load->view('master_layout', $data);
    }

    public function aviso() {
        $data["title"] = "Aviso de Privacidad | Gastos Médicos Mayores";
        $data["view_file"] = "aviso";
        $this->load->view('master_layout', $data);
    }

   public function cotizacion() {

        
        $datos=array(
            'nombre' => $_POST['nombre'],
            'edad' => (int)$_POST['edad'],
            'genero' => $_POST['genero'],
            'sexo' => $_POST['genero'] == "FEMENINO" ? "F" : "M", 
            'ciudad' => (int)$_POST['ciudad'],
            'correo' => $_POST['correo'],
            'telefono' => $_POST['telefono'],
            'opcion' => 'OPCION 1',
            'captcha' => $this->input->post('g-recaptcha-response'),
            'name_ciudad' => $_POST['name_ciudad']
        );

        // var_dump($this->$session->get('user_data'));

        if($this->session->userdata('user_data')){
            $this->session->unset_userdata('user_data');
        }
        
        $this->session->set_userdata('user_data', $datos);

        // var_dump($this->session->userdata('user_data'));

       $d = $this->GNP_model->get_datos($datos);   
       
       // insercion en bases de datos local y crm
       $result = $this->GNP_model->insertaSolicitud($datos);

       // Envio de correo
       $result = $this->mail($datos, $d[0]);

        $data["title"] = "GNP | Cotización";
        $data["datos"] = $d;
        $data["nombre"] = $_POST['nombre'];
        $data["view_file"] = "viewcotizador";

        $this->load->view('master_layout', $data);
       
   }

    public function mail($datos, $d) {

        //configuración de captcha
        // $recaptchaResponse = trim($datos["captcha"]);
        // $userIp=$this->input->ip_address();
        // $secret = $this->config->item('google_secret');

        // $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;
 
        // $ch = curl_init(); 
        // curl_setopt($ch, CURLOPT_URL, $url); 
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        // $output = curl_exec($ch); 
        // curl_close($ch);      
        // $status= json_decode($output, true);

        // var_dump($status);

        // if ($status['success']) {

            $nombre = $datos["nombre"];
            $email = $datos["correo"];
            $telefono = $datos["telefono"];
            $edad = $datos["edad"];
            $genero = $datos["genero"];
            $ciudad = $datos["ciudad"];
            $name_ciudad = $datos["name_ciudad"];
            $anual = $d["anual"];
            $mensual = $d["mensual"];
            
            $this->email->from('ventas@gnpgastosmedicos.com.mx', 'Contacto - Seguro GNP GMM');
            $this->email->to('ventas@gnpgastosmedicos.com.mx');
            //$this->email->to('charlie.carrillo@bluestart.mx');
            // $this->email->cc('charlie.carrillo@bluestart.mx');
            $this->email->set_mailtype("html");

            $this->email->subject('Solicitud de cotización - Seguro GNP GMM');
            $this->email->message('
                        <h3>El siguiente usuario generó una cotización en el portal, si no te ha contactado, sus datos fueron los siguientes:</h3>               
                        <h4>Datos:</h4>
                        <b>Nombre: </b> ' . $nombre . '<br>
                        <b>Email: </b> ' . $email . '<br>
                        <b>Teléfono: </b> ' . $telefono . '<br>
                        <b>Edad: </b> ' . $edad . '<br>
                        <b>Genero: </b> ' . $genero . '<br>
                        <b>Ciudad: </b> ' . $name_ciudad . '<br>
                        <b>Prima Mensual: </b> ' . $mensual . '<br>
                        <b>Prima Anual: </b> ' . $anual . '<br>');

            return $this->email->send();


        // }
        // else{
        //    return false;
        // }
   
    }

}
