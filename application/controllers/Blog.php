<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data["title"] = "Blog | Retiro GNP";
        $data["view_file"] = "blog";
        $this->load->view('master_layout', $data);
    }

    public function millennials() {
        $data["title"] = "Millennials | Blog Retiro GNP";
        $data["view_file"] = "millennials";
        $this->load->view('master_layout', $data);
    }

    // public function consolida() {
    //     $data["title"] = "Consolida | Retiro GNP";
    //     $data["view_file"] = "consolida";
    //     $this->load->view('master_layout', $data);
    // }

    // public function contacto() {
    //    $data["title"] = "Contacto | Retiro GNP";
    //    $data["view_file"] = "contacto";
    //    $this->load->view('master_layout', $data);
    // }


   
}
