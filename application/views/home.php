<header id="page_top" class="home" style="padding-right: 0px; padding-left: 0px; margin-left: 0px; margin-right: 0px;">

    <div class="container" style="padding-right: 0px; padding-left: 0px;">

        <div class="row">

          <div class="form-cover">
                
            <!-- <div class="card"> -->
                
                <div class="card-heading register d-lg-table-cell d-none"></div>
                
                <div class="card-body p-70" style="background-color: #002d74;">

                    <div class="mb-50" style="text-align: center;">
                        <h2 style="font-family: 'Montserrat', sans-serif; color: white;">Cotiza tu Seguro de Gastos Médicos</h2>
                        <!-- <span>Llena los datos y obten tu cotización al momento</span> -->
                    </div>
                    <div class="form-group form-alert">
                        <span></span>
                    </div>
                    <div class="form-group form-alert-success">
                        <span>Recibido. Gracias</span>
                    </div>

                    <form class="ultraform" id="ultraFormRegister" action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads5260406000000530014 method='POST'>
                        

                        <input type='text' style='display:none;' name='xnQsjsdp' value='20608e993632a4f73b4232cd0f1be726bc276948743426578896cf7398542db5'></input> 
                        <input type='hidden' name='zc_gad' id='zc_gad' value=''></input> 
                        <input type='text' style='display:none;' name='xmIwtLD' value='002bb1b98fa2bc25b99c4a2ba788309e4137d9b1e3c80834034961bbf4190e81'></input> 
                        <input type='text'  style='display:none;' name='actionType' value='TGVhZHM='></input>
                         
                        <!-- <input type='text' style='display:none;' name='returnURL' value='https&#x3a;&#x2f;&#x2f;www.aybseguros.com.mx' > </input> -->
                        <input type='text' style='display:none;' name='returnURL' value='http://localhost/pagegnp/'> </input>
                        



                        <div class="row">

                            <div class="col-lg-8">

                                <div class="form-group">
                                    <label for="uf-username">Nombre: <sub>*</sub></label>
                                    <div class="form-file-with-icon field-required">
                                        
                                        <!-- <input class="form-control not-empty" name="username" id="uf-username" type="text" placeholder="Introduce tu nombre"> -->
                                        <input class="form-control not-empty" name="First Name" id="uf-username" type="text" placeholder="Introduce tu nombre">


                                        <i class="far fa-user"></i>
                                        <div class="form-validate-icons">
                                            <span></span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-4">


                                <div class="form-group">
                                    
                                    <label for="uf-gender">Edad <sub>*</sub></label>

                                    <div class="custom-select-wrapper">
                                        
                                        <!-- <select id="uf-gender" class="form-control"> -->
                                        <select id="uf-gender" name="LEADCF51" class="form-control">

                                            <option value="Please Select">Edad</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                        </select>
                                    </div>
                                </div>





                                
                            </div>

                        </div>

                        
                        

                        <div class="row">

                            <div class="col-lg-6">

                                <div class="form-group uf-phone">
                                    
                                    <label for="uf-phone">CP <sub>*</sub></label>
                                    <div class="form-file-with-icon field-required">
                                        
                                        <!-- <input class="form-control not-empty" name="phone" id="uf-phone" type="text" placeholder="Introduce tu teléfono"> -->
                                        <input class="form-control not-empty" name="zip" id="uf-phone" type="text" placeholder="Introduce tu CP">
                                        
                                        <i class="far fa-phone"></i>
                                        <div class="form-validate-icons">
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="uf-gender">Genero <sub>*</sub></label>
                                    <div class="custom-select-wrapper">
                                        
                                        <select id="uf-gender" name="Gender" class="form-control">

                                            <option value="Please Select">Genero</option>
                                            <option value="hombre">Hombre</option>
                                            <option value="mujer">Mujer</option>
                                        </select>
                                    </div>
                                </div>
                            </div>




                            <div class="col-lg-6">
                                <div class="form-group uf-email">
                                    <label for="uf-email">Correo electrónico <sub>*</sub></label>
                                    <div class="form-file-with-icon field-required">
                                        
                                        <!-- <input class="form-control not-empty" name="email" id="uf-email" type="text" placeholder="Introduce tu correo"> -->
                                        <input class="form-control not-empty" name="Email" id="uf-email" type="text" placeholder="Introduce tu correo">

                                        <i class="far fa-envelope"></i>
                                        <div class="form-validate-icons">
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">

                                <div class="form-group uf-phone">
                                    <label for="uf-phone">Teléfono <sub>*</sub></label>
                                    <div class="form-file-with-icon field-required">
                                        
                                        <!-- <input class="form-control not-empty" name="phone" id="uf-phone" type="text" placeholder="Introduce tu teléfono"> -->
                                        <input class="form-control not-empty" name="Phone" id="uf-phone" type="text" placeholder="Introduce tu teléfono">
                                        
                                        <i class="far fa-phone"></i>
                                        <div class="form-validate-icons">
                                            <span></span>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>

                        


                       <div class="form-group">
                            <div class="chek-form">
                                <div class="custome-checkbox">
                                    <input class="form-check-input" type="checkbox" name="checkbox" id="termsCheckBox" value="">
                                    <label class="form-check-label font-md" for="termsCheckBox"><span>Estoy de acuerdo con el <a href="aviso">Aviso de privacidad</a>.</span></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">

                            <button class="btn text-white" style="background-color: #FF5733; font-size: 20px;" type="submit">Cotizar</button>

                        </div>


                        <!-- Do not remove this --- Analytics Tracking code starts -->
                        <script id='wf_anal' src='https://crm.zohopublic.com/crm/WebFormAnalyticsServeServlet?rid=002bb1b98fa2bc25b99c4a2ba788309e4137d9b1e3c80834034961bbf4190e81gid20608e993632a4f73b4232cd0f1be726bc276948743426578896cf7398542db5gid885e3c1045bd9bdcc91bdf30f82b5696gid14f4ec16431e0686150daa43f3210513&tw=61690b96c1d0471b638f31426f38e68aa67fb7ed6da86f32dc10ad817fe55a0a'></script>
                        <!-- Do not remove this --- Analytics Tracking code ends. -->


                    </form>
                    
                </div>

              </div>
          <!-- </div> -->


          <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-lateral-pd ">
                                          <div class="g-recaptcha form-group" data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> 
                                        </div> -->
                                        

            
            <!-- <div class="col-lg-4 col-md-4 hidden-xs hidden-sm">
                
                <div class="header-content">
                    <div class="header-content-inner">
                        <div class="form-container ">

                            <div class="section-heading "> 

                                <ul class="text-left h4 liheigth_list" >
                                    
                                    <li>Suma Asegurada hasta <b>$65,000,000</b></li>
                                    <li>Libre Elección de Médico Tratante</li>
                                    <li>Acceso a Todos los <b>Hospitales a Nivel Nacional</b></li>
                                    <li>Membresía Dental y Médica Móvil</li>
                                    <li>Asistencia en Viajes</li>
                                    <li>Cobertura para Complicaciones del Parto o Cesárea</li>
                                    <li>Check up Dental sin Costo</li>
                                    <li>Video Consulta Ilimitada</li>
                                    
                                </ul>


                            </div>


                        </div>
                    </div>
                </div>

            </div> -->

            <!-- <div class="col-lg-2 col-md-2 hidden-xs hidden-sm">
              
              <img class="img_banner" style="left: 5px; top: 144px !important; position: absolute !important;"  src="<?php echo base_url(); ?>assets/img/dcotor2.png">

            </div> -->

            <!-- <div class="col-lg-5 col-md-5 col-xs-12 col-sm-12">

                <div class="header-content">

                    <div class="header-content-inner">
                        
                        <div class="form-container ">
                            <div class="form-mockup ">
                                
                                  <h1 class="title-mobile">Obtén tu cotización <br/>al instante</h1>
                                <?php 
                                    if(isset($_GET["envio"])){

                                        echo '<br/><label class="text-success text-sm">Tu petición fue enviada, nos pondremos en contacto contigo lo antes posible, gracias.</label>';
                                    } 
                                ?>
                                <form id="cotizar" action="cotizar" method="post">

                                    <input type="hidden" name="from" value="index">

                                    <div class="row ">

                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-lateral-pd ">
                                           <div class="form-group">
                                                <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre" required>
                                            </div>
                                        </div>                                      

                                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 no-lateral-pd ">

                                              <div class="form-group">
                                                <input type="number" name="edad" id="edad" class="form-control" placeholder="Edad" required>
                                              </div>
                                            
                                        </div>
                                      
                                    </div>


                                    <div class="row pd_bttp">

                                      <div class="col-md-6 col-lg-6 col-xs-6  col-sm-6 no-lateral-pd">
                                            
                                            
                                              <select required="required" name="genero" id="genero" class="form-control" data-toggle="select2">
                                                  <option value="" disabled selected="true">Género</option>
                                                  <option value="FEMENINO">Femenino</option>
                                                    <option value="MASCULINO">Masculino</option>
                                                </select>
                                            
                                        </div>


                                        <div class=" col-md-6 col-lg-6 col-xs-6 col-sm-6 no-lateral-pd">
                                           

                                            
                                              <select required="required"  name="ciudad" id="ciudad" class="form-control" data-toggle="select2">
                                                  <option value="" disabled="disabled" selected="true">Ciudad</option>
                                                    <option value="1">CDMX</option>
                                                    <option value="2">MEXICO</option>
                                                    <option value="3">AGUASCALIENTES</option>
                                                    <option value="4">CAMPECHE</option>
                                                    <option value="5">COAHUILA</option>
                                                    <option value="6">PUEBLA</option>
                                                    <option value="7">PACHUCA</option>
                                                    <option value="8">QUERETARO</option>
                                                    <option value="9">NUEVO LEON</option>
                                                    <option value="10">MONTERREY</option>
                                                    <option value="11">MORELOS</option>
                                                    <option value="12">VERACRUZ</option>
                                                    <option value="13">GUADALAJARA</option>
                                                    <option value="14">MICHOACAN</option>
                                                    <option value="15">BAJA CALIFORNIA SUR</option>
                                                    <option value="16">BAJA CALIFORNIA NORTE</option>
                                                </select>
                                           
                                        </div>

                                        <input type="hidden" name="name_ciudad" id="name_ciudad" value="CDMX">
                                      

                                    </div>


                                    <div class="row pd_bttp">


                                      <div class="col-md-6 col-lg-6 col-xs-6  col-sm-6 no-lateral-pd ">
                                           <div class="form-group">
                                                 <input type="email" name="correo" class="form-control" placeholder="Email"  required>
                                            </div>
                                        </div>


                                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 no-lateral-pd ">
                                           <div class="form-group">
                                                <input type="text" name="telefono" class="form-control" placeholder="Teléfono" required>
                                            </div>
                                        </div>


                                        

                                    </div>

                                    <div class="col-sm-offset-2 col-xs-12 col-sm-8 ">
                                      
                                      <p style="color: #fff; font-size: 12px; margin: 0px;" >Al dar clic en cotizar estoy de acuerdo con el <a href="aviso" style="color: #fff;">Aviso de privacidad</a></p> <br/>

                                    </div>
                                        
                                         


                                    <div class="col-sm-offset-2 col-xs-12 col-sm-8 ">
                                        <div class="form-group">
                                            <button id="" type="submit" class="btn btn-lg btn-block btn-default quote_btn " >
                                              Cotiza Gratis <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 ">
                                        <img height="80px" class="" style=""  src="<?php echo base_url(); ?>assets/img/credit_card.png"><br/>
                                        Contrata a 3, 6 y 12 meses sin intereses
                                    </div>

                                    
                                
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div> -->






        </div>
    
    </div>


</header>




<section class="header-content col-sm-12 col-xs-12 hidden-lg hidden-md">
    
    <div class="container" >

      <h1 class=" text-azul-gnp text-sm title-mobile">Beneficios:</h1> <br/>               

        <ul class="text-left h4 text-azul-gnp" style="line-height: 30px;" >
            
            <li>Suma Asegurada hasta <b>$65,000,000</b></li>
            <li>Libre Elección de Médico Tratante</li>
            <li>Acceso a Todos los <b>Hospitales a Nivel Nacional</b></li>
            <li>Membresía Dental y Médica Móvil</li>
            <li>Asistencia en Viajes</li>
            <li>Cobertura para Complicaciones del Parto o Cesárea</li>
            <li>Check up Dental sin Costo</li>
            <li>Video Consulta Ilimitada</li>
            
        </ul>

    </div>
    
</section>





<section id="offer" class="offer" style="background-color: white !important;">
    
        <div class="container" style="padding-left: 30px; padding-right: 30px;">

          <div class="row text-center ">

            <h1 class=" text-azul-gnp text-sm title-mobile">¿QUE INCLUYE MI SEGURO DE GASTOS MÉDICOS MAYORES ?</h1>                
            

              <div class="col-md-4 col-sm-12">
                  <div class="blog_worp">
                      <div class="blog_img">
                          <a class="imghover" href="#"><img class="img-responsive" alt="Blog Img 01" src="<?php echo base_url(); ?>assets/img/gnp3.jpg"></a>
                      </div>
                      <div class="blog_content">
                          <h2><a class="title" href="#">La mejor red de Hospitales y Médicos</a></h2>
                      </div>
                  </div>
              </div>

              <div class="col-md-4 col-sm-12">
                  <div class="blog_worp">
                      <div class="blog_img">
                          <a class="imghover" href="#"><img class="img-responsive" alt="Blog Img 01" src="<?php echo base_url(); ?>assets/img/gnp1.jpg"></a>
                      </div>
                      <div class="blog_content">
                          <h2><a class="title" href="#">Hospitalización en cuarto privado</a></h2>
                          
                      </div>
                  </div>
              </div>

              <div class="col-md-4 col-sm-12">
                  <div class="blog_worp">
                      <div class="blog_img">
                          <a class="imghover" href="#"><img class="img-responsive" alt="Blog Img 01" src="<?php echo base_url(); ?>assets/img/gnp2.jpg"></a>
                      </div>
                      <div class="blog_content">
                          <h2><a class="title" href="#">Honorarios médicos y de enfermeras</a></h2>
                      </div>
                  </div>
              </div>

          </div>

          <div class="row text-center padding-top-50">
            <div class="col-md-4 col-sm-12">
                  <div class="blog_worp">
                      <div class="blog_img">
                          <a class="imghover" href="#"><img class="img-responsive" alt="Blog Img 01" src="<?php echo base_url(); ?>assets/img/medico_movil.jpg"></a>
                      </div>
                      <div class="blog_content">
                          <h2><a class="title" href="#">Membres&iacute;a m&eacute;dica m&oacute;vil</a></h2>
                      </div>
                  </div>
              </div>

              <div class="col-md-4 col-sm-12">
                  <div class="blog_worp">
                      <div class="blog_img">
                          <a class="imghover" href="#"><img class="img-responsive" alt="Blog Img 01" src="<?php echo base_url(); ?>assets/img/bebe.jpg"></a>
                      </div>
                      <div class="blog_content">
                          <h2><a class="title" href="#">Cobertura del reci&eacute;n nacido</a></h2>
                          
                      </div>
                  </div>
              </div>

              <div class="col-md-4 col-sm-12">
                  <div class="blog_worp">
                      <div class="blog_img">
                          <a class="imghover" href="#"><img class="img-responsive" alt="Blog Img 01" src="<?php echo base_url(); ?>assets/img/dental.jpg"></a>
                      </div>
                      <div class="blog_content">
                          <h2><a class="title" href="#">Membresías Dentalia</a></h2>
                      </div>
                  </div>
              </div>


          </div>

        </div>

    </div>
    
</section>



<section id="info" class="offer black" style="background-color: white !important;">
      <div class="container" >
        <div class="title-wrap text-center">


          <h1 class="text-azul-gnp text-sm title-mobile">¿QUÉ DEBO SABER DE UNA PÓLIZA DE GASTOS MÉDICOS MAYORES?</h1>
          
          <div class="h-decor"></div>
        </div>



        
        <div class="row d-block">

            <div class="col-md-4">
            <div class="counter-box bg-card-blue">
              <div class="counter-box-icon"><i class="fa fa-hand-stop-o"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <div class="counter-box-text"><h3>Coaseguro</h3></div>
              <p><h4>Es el porcentaje que se aplica al monto total de los gastos en que se incurre durante un accidente o enfermedad, una vez descontado el deducible<h4></p>
            </div>
          </div>



          <div class="col-md-4">
            <div class="counter-box bg-card-orange">
              <div class="counter-box-icon"><i class="fa fa-dollar"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <div class="counter-box-text"><h3>Suma Asegurada</h3></div>
              <p><h4>Cantidad máxima por la cual se cubre una enfermedad o accidente<h4></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="counter-box bg-card-blue">
              <div class="counter-box-icon"><i class="fa fa-sort-amount-desc"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <div class="counter-box-text"><h3>Deducible</h3></div>
              <p><h4>Es una cantidad fija a cargo del asegurado que se cubrirá cuando se presente una enfermedad o un accidente, la cual no es reembolsable<h4></p>
            </div>
          </div>
        </div>

        <div class="row d-block">
          <div class="col-md-4">
            <div class="counter-box bg-card-orange">
              <div class="counter-box-icon"><i class="fa fa-bed"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <div class="counter-box-text"><h3>Padecimientos Preexistentes</h3></div>
              <p><h4>Son aquellos padecimientos que hayan sido declarados por el asegurado antes de la contratación de la póliza<h4></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="counter-box bg-card-blue">
              <div class="counter-box-icon"><i class="fa fa-clock-o"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <div class="counter-box-text"><h3>Periodo de Espera</h3></div>
              <p><h4>Existen enfermedades que debido a su frecuencia requieren un determinado tiempo para poder contar con el servicio de cobertura. Este periodo garantiza que cuando se presente, ya exista la antiguedad y monto necesario<h4></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="counter-box bg-card-orange">
              <div class="counter-box-icon"><i class="fa fa-hospital-o"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <div class="counter-box-text"><h3>Nivel Hospitalario</h3></div>
              <p><h4>Es la red de Hospitales que el asegurado podrá elegir en el momento de la contratación para su atención médica<h4></p>
            </div>
          </div>

          
        </div>
        

      </div>
    
</section>


<section id="info" class="offer black bg-section-orange" >
      <div class="container" style="padding: 30px;" >
        <div class="title-wrap text-center" style="padding-bottom: 30px;">
          <h1 class="text-sm title-mobile">¿POR QUÉ GNP ES LA MEJOR ASEGURADORA DE GASTOS MÉDICOS MAYORES EN MÉXICO?</h1>
          
        </div>

        <div class="row d-block">

            <div class="col-md-4">
            <div class="counter-box2" style="">
              <div class="counter-box-icon"><i class="fa fa-line-chart"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <h4>Solidez Financiera con calificaciones que nos avalan a nivel mundial por AM Best Company<h4>

            </div>
          </div>



          <div class="col-md-4">
            <div class="counter-box2">
              <div class="counter-box-icon"><i class="fa fa-globe"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <h4>Encabezamos el mercado a nivel nacional<h4>
            </div>
          </div>

          <div class="col-md-4">
            <div class="counter-box2">
              <div class="counter-box-icon"><i class="fa fa-handshake-o"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
             <h4>Contamos con alianzas con proveedores internacionales como: Mayo Clinic, Gleveland(#1 en Cáncer), Aetna y Gloabl Excel<h4>
            </div>
          </div>
        </div>

        <div class="row d-block">
          <div class="col-md-4">
            <div class="counter-box2">
              <div class="counter-box-icon"><i class="fa fa-h-square"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <h4>Tenemos convenio con Hospitales nacionales que cuentan con tecnología de punta<h4>
            </div>
          </div>

          <div class="col-md-4">
            <div class="counter-box2">
              <div class="counter-box-icon"><i class="fa fa-user-md"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <h4>Brindamos acceso a una amplia red de médicos para cualquier especialidad<h4>
             </div>
          </div>

          <div class="col-md-4">
            <div class="counter-box2">
              <div class="counter-box-icon"><i class="fa fa-plus-square"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <h4>Más de 14 mil 800 mpd pagados en servicios de salud a nuestros asegurados<h4>
              
            </div>
          </div>

          
        </div>
        

      </div>
    
</section>


<section id="info" class="offer black bg-section-blue" >
      <div class="container" style="padding: 30px; " >
        <div class="title-wrap text-center" style="padding-bottom: 30px;">
          <h1 class="text-sm title-mobile">¿CÓMO ELEGIR MI SEGURO DE GASTOS MÉDICOS MAYORES?</h1>
          
        </div>

        <div class="row d-block">

            <div class="col-md-4">
            <div class="counter-box2" style="">
              <div class="counter-box-icon"><i class="fa fa-money"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
              <h4>¿Cu&aacute;nto puedes designar en el momento para cubrir el deducible en el momento de una enfermedad o accidente?<h4>

            </div>
          </div>



          <div class="col-md-4">
            <div class="counter-box2">
              <div class="counter-box-icon"><i class="fa fa-stethoscope"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
             <h4>¿A qu&eacute; niveles de Hospitales asisto en el momento de una atenci&oacute;n m&eacute;dica?<h4>
            </div>
          </div>

          <div class="col-md-4">
            <div class="counter-box2">
              <div class="counter-box-icon"><i class="fa fa-question-circle"></i></div>
              <!-- <div class="counter-box-number"><span class="count" data-to="4" data-speed="1500">0</span></div> -->
              <div class="decor"></div>
            <h4>¿Cu&aacute;nto pagas normalmente por una consulta de especialidad m&eacute;dica?<h4>
            </div>
          </div>
        </div>

      

          
        </div>
        

      </div>
    
</section>

<section id="info" class="offer" style="background-color: white !important;" >
  <div class="container" style="padding: 30px;" > 
    <div class="row black">
      <div class="col-md-12 text-left">

        
        <div style="padding-bottom: 20px;">

          <h1 class=" text-azul-gnp text-sm title-mobile">¿POR QU&Eacute; CONTRATAR  UN SEGURO DE GASTOS M&Eacute;DICOS MAYORES?</h1>


        </div>

          <p><font size="4">Sabemos que lo m&aacute;s importante es tu salud y la de tu familia, y en caso de enfrentarte a una situaci&oacute;n que la ponga en riesgo no escatimar&aacute;s ning&uacute;n recurso para preservarla.</font></p>

          <p><font size="4">Sin embargo, en ocasiones la atenci&oacute;n de una enfermedad o accidente puede superar la capacidad econ&oacute;mica de la familia e incluso poner en riesgo tu patrimonio.</font></p>

          <p><font size="4">Contar con el apoyo y respaldo financiero de un seguro de Gastos M&eacute;dicos Mayores es fundamental ya que te brinda la tranquilidad de saber que, en caso de una enfermedad o accidente podr&aacute;s hacer frente a estos imprevistos.</font></p>

          <p><font size="4">En 2019, GNP pago m&aacute;s de 8 mil MDP en servicios de salud, por la atenci&oacute;n de 80 mil 400 reclamaciones, esto representa 23 millones de pesos diariamente.</font></p>

          <h3>Padecimientos de mayor importe pagados:</h3>

      <div class="table-responsive">
        <table >
          <tr style="background-color:#00337F; color: white;">
            <th>Padecimiento</th>
            <th>Sexo</th>
            <th>Edad</th>
            <th>Ciudad</th>
            <th>Importe en M.M</th>
          </tr>
          <tr>
            <td>Leucemia Linfoife</td>
            <td>Mujer</td>
            <td>51</td>
            <td>Puebla</td>
            <td>$28,134,714</td>
          </tr>
          <tr>
            <td>Complicaci&oacute;n de Ces&aacute;ria</td>
            <td>Mujer</td>
            <td>40</td>
            <td>Aguascalientes</td>
            <td>$21,556,240</td>
          </tr>
          <tr>
            <td>Transpocisi&oacute;n de grandes vasos</td>
            <td>Hombre</td>
            <td>0</td>
            <td>Torre&oacute;n</td>
            <td>$19,256,736</td>
          </tr>
          <tr>
            <td>Neumon&iacute; Alveolar</td>
            <td>Mujer</td>
            <td>58</td>
            <td>Los Mochis</td>
            <td>$8,232,345</td>
          </tr>
          <tr>
            <td>Purpura</td>
            <td>Hombre</td>
            <td>22</td>
            <td>Monterrey</td>
            <td>$5,726,534</td>
          </tr>
        </table>
      </div>
    </div>
    </div>

  </div>
</section>




<!--

    
            



  -->