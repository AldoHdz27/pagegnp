<div class="header-spacer" id="nosotros"></div>

<div class="content-wrapper">

    <!-- Contacts -->

    <section>
        <div class="container-fluid no-padding">



            <div class="col-lg-6 col-lg-offset-1 col-md-12 col-sm-12 pt100 pb100">

                <div class="crumina-module crumina-heading">
                    <h3 class="heading-title">Mensaje enviado con éxito, te contactaremos a la brevedad.</h3>
                </div>
                <div class="widget w-contacts w-contacts--style2 ">

                    <a href="index">
                        <button class="btn btn--grey " type="submit">
                            Regresar
                        </button>
                    </a>


                </div>


            </div>

        </div>
    </section>

    <!-- ... end Contacts -->

</div>