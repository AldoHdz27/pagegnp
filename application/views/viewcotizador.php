<?php
setlocale(LC_MONETARY, 'es_MX');
?>

<header id="page_top" class="home5">

	<div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="header-content">
                    <div class="header-content-inner2">
                    	<div class="form-container ">
                            <div class="form-mockup " style="border-radius: 5px;">
                                <form  method="post">

                                	<div class="form-container ">

                                        <div class="section-heading text-azul-gnp black" style="padding: 30px;">               

                                            <input type="text" name="nombre" value="<?php echo $nombre ; ?>" hidden>
                                            <p><font size="6" class="uppercase"><b><?php echo $nombre; ?></b></font></p>

                                            <p>Gracias por cotizar tu seguro de <b>Gastos Médicos con GNP</b>.</p>

                                            <p>En breve un asesor especializado se pondra en contacto contigo para explicarte todos los beneficios de tu póliza.</p>

                                            <p>Con una aportacion mensual de <b><?php echo '$'.number_format($datos[0]['mensual'],2,'.',','); ?></b> , podrás protegerte ante una enfermedad o acidente:</p>

                                            <div class="col-lg-offset-1 col-xs-12 col-sm-12 " style="text-align: left;">

                                                <ul class="" style="line-height: 30px;" >
            
                                                    <li>Acceso a todos los hospitales de México, incluye los de alta tecnologia como ABC y Angeles</li>

                                                    <li>Honorarios Médicos</li>
                                                    <li>Medicamentos</b></li>
                                                    <li>Check up dental Básico</li>
                                                    <li>Video consulta medico general sin costo</li>

                                                    <li>Cero deducible por accidente en algunos hospitales</li>
                                                    <li>Ambulancia terrestre y aerea</li>
                                                    <li>Médico a domicilio a costo preferente</li>


                                                    <li>Cirugia Robótica</li>
                                                    <li>Estudios de laboratario</li>
                                                    <li>Convenio con ABC</li>
                                                    <li>Y muchos beneficios mas</li>
                                                    
                                                </ul>
                                                
                                            </div>



                                            

                                             <p>Además, si pagas de contado <b><?php echo '$'.number_format($datos[0]['anual'],2,'.',','); ?></b> , pueden hacerlo a 3, 6 y 12 meses sin intereses (Pregunta por nuestras tarjetas paticipantes)<b></p>


                                            
                                            <!-- <p>Asegurar tu tranquilidad en caso de requerir atención médica, hoy es posible con  Vérsatil.</p>

                                            <p>Con una aportacion de mensual de <b><?php echo '$'.number_format($datos[0]['mensual'],2,'.',','); ?></b> , podrás asegurar la tranquilidad de saber que tú y los tuyos estarán protegidos <b>pase lo que pase!</b></p>

                                            <p>También puedes pagar de contado <b><?php echo '$'.number_format($datos[0]['anual'],2,'.',','); ?></b> , a 6 meses sin intereses con: Banamex, BBVA, AMEX, HSBC, Santander y Banorte<b></p>

                                            <p>Un plan de Gastos Médicos Mayores que te ofrece protección financiera para hacer frente a eventualidades médicas por enfermedades y/o accidentes, brindádote acceso a cualquier hospital a nivel nacional y libre elección del médico tratante.</p> -->

                                        </div>

                                    </div>


                        <input type="text" value="<?php echo $nombre; ?>" id="nombre1" hidden>
                        <input type="text" 
                        value="<?php echo '$'.number_format($datos[0]['mensual'],2,'.',','); ?>" 
                        id="mensual" hidden >
                        <input type="text" 
                        value="<?php echo '$'.number_format($datos[0]['anual'],2,'.',','); ?>" 
                        id="anual" hidden >

                        	
                                        <div class="row">

                                            <div class="col-sm-offset-3 col-lg-6 col-sm-6 black">



                                                <div class="row">

                                                    <h1 class="text-azul-gnp">Deseo contratar: </h1>
                                                    <br/>
                                                    <span class="text-azul-gnp glyphicon glyphicon-shopping-cart" style="font-size: 30px"></span>

                                                    
                                                </div>

                                                <div class="row" style="padding-top: 30px;">


                                                <div class="col-lg-6 col-sm-6 col-xs-6 ">
                                                    <div class="form-group">
                                                        <button style="background: #00337F;;" type="button" class="btn btn-default"><a href="tel:5611090653" target="_blank"><font color="white"><b> Teléfono</b></font></a></button>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-sm-6 col-xs-6">

                                                        <button style="background: #00337F;;" type="button" class="btn btn-default"><a href="https://wa.me/525611090653?text=Hola! Acabo de cotizar un seguro de gastos médicos y deseo contratar" target="_blank"><font color="white"><b> WhatsApp</b></font></a></button>
                                                    </div>
                                                    
                                                </div>
                                                    
                                                </div>

                                                
                                                
                                                <div class="form-group">
                                                    
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                        
                    </div>
                </div>
            </div>
           

</header>
