
<!DOCTYPE html>
<html lang="es">
    <?php $this->load->view("Elements/page_header", array("title" => $title)); ?>

    <body id="page-top">
        
        <?php 
        if($view_file == "viewcotizador"){
        ?>
        <!-- Event snippet for Enviar formulario de clientes potenciales conversion page -->
        <script>
          gtag('event', 'conversion', {'send_to': 'AW-791756315/rn1LCM7RgcIBEJv8xPkC'});
        </script>
        <?php 
        }
        ?>
        
        <!--  Clickcease.com tracking-->
        <script type='text/javascript'>var script = document.createElement('script');
        script.async = true; script.type = 'text/javascript';
        var target = 'https://www.clickcease.com/monitor/stat.js';
        script.src = target;var elem = document.head;elem.appendChild(script);
        </script>
        <noscript>
        <a href='https://www.clickcease.com' rel='nofollow'><img src='https://monitor.clickcease.com/stats/stats.aspx' alt='ClickCease'/></a>
        </noscript>
        <!--  Clickcease.com tracking-->
        
        <?php $this->load->view("Elements/page_whatsapp");?>
        
        <?php $this->load->view("Elements/page_navigator", array("view_file" => $view_file)); ?>                

        <?php $this->load->view($view_file); ?>

        <?php $this->load->view("Elements/page_footer"); ?>

        <?php $this->load->view("Elements/page_scripts"); ?>

    </body>

    
    <script>
        function gtag_report_conversion(url) {

          console.log("gtag_report_conversion");
          
          var callback = function () {
            if (typeof(url) != 'undefined') {
              window.location = url;
            }
          };

          gtag('event', 'conversion', {
              'send_to': 'AW-791756315/rn1LCM7RgcIBEJv8xPkC',
              'event_callback': callback
          });

          return false;

        }
    </script>


    <?php 

    if($view_file == "viewcotizador"){

        // var_dump($this->session->userdata('user_data'));

        if($this->session->userdata('user_data')){
            
            $datos = $this->session->userdata('user_data');

            $nombre = $datos["nombre"];
            $sexo = $datos["genero"];
            $edad = $datos["edad"];
            $cp = "00000";
            $telefono = $datos["telefono"];
            $correo = $datos["correo"];
            $producto = "GastosMedicos";

            // $sexo = "Femenino";
            // if($genero == "M"){
            //     $sexo = "Masculino";
            // }

            $tipo = "GMM";
            $pagina = "GMM2";

            $this->session->unset_userdata('user_data');  


    ?>
    
            <script type="text/javascript">

                console.log("send Soho request");

                var name = "<?php echo $nombre; ?>";
                var telefono = "<?php echo $telefono; ?>";
                var edad = "<?php echo $edad; ?>";
                var genero = "<?php echo $sexo; ?>";
                var email = "<?php echo $correo; ?>";
                var producto = "<?php echo $producto; ?>";
                var tipo = "<?php echo $tipo; ?>";
                var pagina = "<?php echo $pagina; ?>";

                var data = {
                    "name": name,
                    "telefono": telefono,
                    "edad": edad,
                    "genero": genero,
                    "email": email,
                    "producto": producto,
                    "tipo": tipo,
                    "pagina": pagina
                }
                
                console.log(data);

                $.ajax({
                    url: "https://flow.zoho.com/774907168/flow/webhook/incoming?zapikey=1001.1bfbb7d8cffe49d59d478dd525cfbbbf.7cbe7bd4364e2c2a6c49a2049c794638&isdebug=false",
                    type: 'POST',
                    dataType: 'json',
                    async : true,
                    data: data,
                    crossDomain: true,
                    success: function(resp) {

                        console.log(resp);

                    },
                    error: function(request,err){

                        console.log("Error receiving soho response")
                        console.log(err)
                        console.log("Request: "+JSON.stringify(request));

                    }
                });
                
            </script>

        <?php 
        }
        ?>

    <?php
    }
    ?>


</html>

