<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 social_touch text-center">
                <ul class="list-inline list-social">
                    
                    <!-- <li class="social-twitter">
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="social-fa-linkedin">
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                    </li> -->
                </ul>
            </div>
            <div class="col-sm-12 copy_right text-center">
                <p>GNP Gastos Médicos Mayores.</p>
                <p class="txt-md"><i class="fa fa-phone" aria-hidden="true"></i>
                Teléfono: 56-1109-0653</p>
                <!--
                <p class="txt-sm">GNP Seguros de Retiro is proudly powered by <a class="text-white" href="http://www.bluestart.mx" target="_blank">BlueStart</a>.</p>
                -->
            </div>
        </div>
    </div>
</footer>
<!-- End Of Footer -->


<!-- Back To Top -->
<a id="back-to-top" href="#page-top" class="btn btn-primary btn-lg back-to-top page-scroll" role="button"><span class="fa fa-long-arrow-up"></span><br>inicio</a>
<!-- End Back To Top -->