<div id="" class="navbar ">

    	<div class="">

    		<div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 ">

	            <p style="font-size: 9px; margin-bottom: 0px; padding:0px; color:#00337F; text-align: center;">Agente autorizado</p>

	        	<a class="navbar-brand page-scroll text-center" style="text-align: center;" href="home">
	                <img class="text-center" width="120px" style="margin-left: 10px;" src="<?php echo base_url(); ?>assets/img/custom/logo_gnp.png" alt="LOGO">
	            </a>
			        
			</div>

        	<div class="hidden-xs hidden-sm col-lg-6 col-md-6 text-center ">

        		<h1 class="text-azul-gnp" style="font-size: 30px; padding-top: 10px; font-family: 'Montserrat', sans-serif; font-weight: 600;" >GASTOS MÉDICOS MAYORES</h1>

        	</div>


        	<div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 pull-right" style="">

        		<!-- <label class="tel_nav">&nbsp;56-11090653</label> -->
        		 
    		 	<a href="tel:5611090653" class="">
    		 		<i  class="fa fa-phone " aria-hidden="true" style="color: #FF692D; font-size: 20px;">
    		 			<label class="tel_nav" style="font-size: 20px; font-family: 'Montserrat', sans-serif; font-weight: 700; padding-top: 20px;">&nbsp;56-11090653</label>
    		 		</i>
    		 	</a>
        		 
        		 <!-- <h1 class="hidden-sm tel_nav" ><i class="fa fa-phone fa-2x" aria-hidden="true" style="color: #FF692D"></i>&nbsp;&nbsp;&nbsp;<u>55-3965-6682</u></h1> -->
        	</div> 
    		
    	</div>
        


</div>
<!-- End Of Nav Bar -->