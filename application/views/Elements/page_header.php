<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Cotiza tu Seguro de gastos Médicos Mayores de GNP al instante.">
    <meta name="author" content="">

    <!-- <meta http-equiv="cache-control" content="max-age=0" /> -->
    <!-- <meta http-equiv="cache-control" content="no-cache" /> -->
    <!-- <meta http-equiv="expires" content="0" /> -->
    <!-- <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" /> -->
    <meta http-equiv="pragma" content="no-cache" />

    <title><?php echo $title;?></title>


    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/custom/logo_gnp_sm.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/custom/logo_gnp_sm.png" type="image/x-icon">



    
    <!-- Custom Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">  -->

        <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Plugin CSS -->
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">  -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/simple-line-icons/css/simple-line-icons.css">

    <!-- Preloader CSS -->
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/preloader.css"> -->
    <!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" /> -->

    <!-- <link href="<?php echo base_url(); ?>assets/libs/jquery-nice-select/nice-select.css" rel="stylesheet" type="text/css" /> -->

    <!-- <link href="<?php echo base_url(); ?>assets/libs/select2/select2.min.css" rel="stylesheet" type="text/css" /> -->

    
    <!-- Responsive CSS -->
    <!-- <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet"> -->

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">

    <!-- <link href="<?php echo base_url(); ?>assets/libs/selectpicker/css/bootstrap-select.min.css" rel="stylesheet"> -->



    
    <!-- <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet"> 

    <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Catamaran|Comfortaa|Poppins" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@300;400&display=swap" rel="stylesheet"> 

    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@300&display=swap" rel="stylesheet">  -->


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_form/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_form/css/style-1.css">



    <!-- <link href="<?php echo base_url(); ?>assets/css/icons.min.css" rel="stylesheet" type="text/css" /> -->
    
    <!-- <link href="<?php echo base_url(); ?>assets/css/app.min.css" rel="stylesheet" type="text/css" /> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php $this->load->view("Elements/page_analytics");?>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    
</head>