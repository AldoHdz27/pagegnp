


<!-- Section Blog -->
<section id="blog" class="blog" style="margin-top: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <div class="section-heading">
                    <h2>Aviso de privacidad</h2>
                    <p style=" text-align: justify;">En cumplimiento a las disposiciones de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares (LFPDPPP), <strong>Ana Laura Alvarado Olvera</strong> con nombre comercial “AB Asesores en Seguros y Fianzas”, <a href="https://appagentes.gnp.com.mx/tarjeta/digital-card/profile/79f87239-2627-40b2-858a-949b32c967a3?t=1663729414873">agente autorizado</a> de conformidad con el artículo 14 de la Ley Sobre el Contrato de Seguro, de <strong>Grupo Nacional Provincial S.A.B. (GNP)</strong>, con domicilio en Santa Fe, número 27, Lomas de Capistrano Atizapan de Zaragoza, Estado de México, C.P. 52987 hace de su conocimiento que los datos personales que sean recabados y/o proporcionados a través de cuestionarios o formatos del seguro por vía electrónica, óptica, sonora, visual o por cualquier otro medio o tecnología, y aquellos generados con motivo de la relación jurídica que tengamos celebrada, o que en su caso, se celebre con Usted, serán tratados para las siguientes finalidades:</p>

                    <p style=" text-align: justify;"><strong>Estimado Solicitante, Contratante, Aseguradoy/o Beneficiario</strong>. Sus datos personales que acaba de proporcionar mediante el llenado del cuestionario que fue puesto a su disposición se incluyen datos de identificación, patrimoniales, financieros, procesos legales en los que participe o haya participado, datos sensibles relativos a su estado de salud, información de pago, como nombre, número de tarjeta de crédito, fecha de caducidad, dirección de facturación, preferencia sexual, características personales y características físicas serán tratados para evaluar y emitir sus solicitudes de seguro, dar trámite a sus reclamaciones de siniestros, cobrar las primas del seguro, mantener o renovar sus pólizas de seguro; así como para todos los fines relacionados con el cumplimiento de nuestras obligaciones de conformidad con lo establecido en la Ley Sobre el Contrato de Seguro y en la normatividad vigente.</p>

                    <p style=" text-align: justify;">De la misma manera se informa que sus datos personales podrán ser tratados para finalidades secundarias como son el ofrecimiento y promoción de bienes, productos y servicios y/o prospección comercial. Si Usted no desea recibir alguno de los ofrecimientos descritos en este párrafo, puede manifestar su negativa de la siguiente forma:</p>

                    <p style=" text-align: justify;">Una vez proporcionada la información por cualquiera de nuestros medios, es decir a través de medios electrónicos, ópticos o sonoros, enviando un correo electrónico a la dirección electrónica <a href="mailto:avp@aybseguros.com.mx"> avp@aybseguros.com.mx </a> cuya solicitud sera atendida en un plazo no mayor a 24 horas.</p>

                    <p style=" text-align: justify;">Usted podrá ejercer sus derechos ARCO, (Acceso, Rectificación, Cancelación, Oposición), de Revocación del consentimiento y limitación de uso de sus datos, mediante solicitud escrita a la dirección electrónica <a href="mailto:avp@aybseguros.com.mx"> avp@aybseguros.com.mx </a> Lo anterior está sujeto a que el ejercicio de dichos derechos no obstaculice el cumplimiento de alguna Ley vigente o mandato judicial así como para dar cumplimiento a las obligaciones derivadas de la relación jurídica entre Usted y <strong>Ana Laura Alvarado Olvera</strong> con nombre comercial “Asesores en Seguros y Fianzas”.</p>

                    <p style=" text-align: justify;">Por favor, tome en cuenta que los mensajes enviados electrónicamente pueden ser manipulados o interceptados, por lo tanto <strong>Ana Laura Alvarado Olvera</strong> con nombre comercial “Asesores en Seguros y Fianzas” no se hace responsable si los mensajes llegan incompletos, retrasados, son eliminados o contienen algún programa malicioso (virus informático).</p>


                    <p style=" text-align: justify;">Para cualquier asunto relacionado con este Aviso de Privacidad y sobre el tratamiento de sus datos personales, puede contactarnos en la direccion electrónica <a href="mailto:avp@aybseguros.com.mx"> avp@aybseguros.com.mx </a>  y vía telefónica (55) 5887-3396 así como en nuestra página de internet <a href="www.gnpgastosmedicos.com.mx"> www.gnpgastosmedicos.com.mx </a>  </p>

                    <p style=" text-align: justify;">El presente Aviso de Privacidad, así como sus modificaciones, estarán a su disposición en la página de Internet <a href="www.gnpgastosmedicos.com.mx"> www.gnpgastosmedicos.com.mx </a>  y colocado en nuestra oficina.</p>

                    <hr/>

                    <p style=" text-align: right;">Fecha de última modificación: Septiembre 2022.</p>




                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Of Section Blog -->

